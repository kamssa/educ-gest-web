package ci.kossovo.educ.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import ci.kossovo.educ.EducGestMetierDaoJpaApplication;

@SpringBootApplication
@Import(EducGestMetierDaoJpaApplication.class)
public class EducGestWebApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EducGestWebApiApplication.class, args);
	}
}
